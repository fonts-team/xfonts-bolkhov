Source: xfonts-bolkhov
Maintainer: Anton Zinoviev <zinoviev@debian.org>
Section: x11
Priority: optional
Standards-Version: 3.5.2.0
Build-Depends: debhelper (>=3), trscripts (>= 1.10), xutils
define(`bolkhov75dpi', `
Package: xfonts-bolkhov$1-75dpi
Architecture: all
Description: 75 dpi $2 Cyrillic fonts for X (Cyr-RFX collection)
 This package provides a set of bitmapped Cyrillic fonts at 75 dots
 per inch.  These are $3 fonts.
 In most cases it is desirable to have the X font server (xfs) and/or
 an X server installed to make the fonts available to X clients.
 .
 The fonts from this package are not designed for big monitors and/or
 large screen resolutions (over 1024x768).  You can find big fonts in
 the package xfonts-cronyx$1-100dpi.
 .
 The fonts are modified (mainly with Cyrillics added) versions of the
 most important X-Window fonts from 75dpi/.
 .
 Upstream author: Dmitry Bolkhovityanov <bolkhov@inp.ins.su>
Section: x11
Priority: optional
Replaces: xfonts-bolkhov-$4
Depends: xutils (>= 4.0.2-11)
Suggests: xfs | xserver
Conflicts: xfonts-bolkhov-$4')dnl
define(`bolkhovmisc', `
Package: xfonts-bolkhov$1-misc
Architecture: all
Description: Character-cell $2 Cyrillic fonts for X (Cyr-RFX collection)
 This package provides a standard set of character-cell low-resolution
 Cyrillic fonts.  These are $3 fonts.
 In most cases it is desirable to have the X font server (xfs) and/or
 an X server installed to make the fonts available to X clients.
 .
 The fonts are modified (mainly with Cyrillic added) versions of the
 standard X-Window fonts from misc/.  Upstream author: Dmitry
 Bolkhovityanov <bolkhov@inp.ins.su>
Section: x11
Priority: optional
Replaces: xfonts-bolkhov-$4
Depends: xutils (>= 4.0.2-11)
Suggests: xfs | xserver
Conflicts: xfonts-bolkhov-$4')dnl
bolkhov75dpi(`', `Unicode', `Unicode (ISO10646-1)', `nop')
bolkhov75dpi(`-cp1251', `CP1251 encoded', `CP1251 (microsoft-cp1251)', `cp1251')
bolkhov75dpi(`-isocyr', `ISO 8859-5 encoded', `ISO 8859-5', `iso')
bolkhov75dpi(`-koi8r', `KOI8-R encoded', `KOI8-R', `koi8r')
bolkhov75dpi(`-koi8u', `KOI8-U encoded', `KOI8-U', `koi8u')
bolkhovmisc(`', `Unicode', `Unicode (ISO10646-1)', `nop')
bolkhovmisc(`-cp1251', `CP1251 encoded', `CP1251 (microsoft-cp1251)', `cp1251')
bolkhovmisc(`-isocyr', `ISO-8859-5 encoded', `ISO-8859-5', `iso')
bolkhovmisc(`-koi8r', `KOI8-R encoded', `KOI8-R', `koi8r')
bolkhovmisc(`-koi8u', `KOI8-U encoded', `KOI8-U', `koi8u')
